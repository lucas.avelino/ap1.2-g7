﻿
namespace reservationSalles2018
{
    partial class frmStatistiquesSalles
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.lblDate = new System.Windows.Forms.Label();
            this.cbxMois = new System.Windows.Forms.ComboBox();
            this.lblSéparationDate = new System.Windows.Forms.Label();
            this.DGVStatistiqueSalles = new System.Windows.Forms.DataGridView();
            this.Salle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Nombre = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.MontantGlobal = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbxAnnee = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.btnAfficher = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.DGVStatistiqueSalles)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(392, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(323, 33);
            this.label1.TabIndex = 73;
            this.label1.Text = "Statistiques par salles";
            // 
            // lblDate
            // 
            this.lblDate.AutoSize = true;
            this.lblDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDate.Location = new System.Drawing.Point(461, 95);
            this.lblDate.Name = "lblDate";
            this.lblDate.Size = new System.Drawing.Size(53, 18);
            this.lblDate.TabIndex = 82;
            this.lblDate.Text = "Mois : ";
            // 
            // cbxMois
            // 
            this.cbxMois.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxMois.FormattingEnabled = true;
            this.cbxMois.Items.AddRange(new object[] {
            "01",
            "02",
            "03",
            "04",
            "05",
            "06",
            "07",
            "08",
            "09",
            "10",
            "11",
            "12"});
            this.cbxMois.Location = new System.Drawing.Point(518, 94);
            this.cbxMois.Name = "cbxMois";
            this.cbxMois.Size = new System.Drawing.Size(52, 21);
            this.cbxMois.TabIndex = 1;
            // 
            // lblSéparationDate
            // 
            this.lblSéparationDate.AutoSize = true;
            this.lblSéparationDate.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSéparationDate.Location = new System.Drawing.Point(576, 95);
            this.lblSéparationDate.Name = "lblSéparationDate";
            this.lblSéparationDate.Size = new System.Drawing.Size(12, 18);
            this.lblSéparationDate.TabIndex = 82;
            this.lblSéparationDate.Text = "/";
            // 
            // DGVStatistiqueSalles
            // 
            this.DGVStatistiqueSalles.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGVStatistiqueSalles.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Salle,
            this.Nombre,
            this.MontantGlobal});
            this.DGVStatistiqueSalles.Location = new System.Drawing.Point(46, 131);
            this.DGVStatistiqueSalles.Name = "DGVStatistiqueSalles";
            this.DGVStatistiqueSalles.Size = new System.Drawing.Size(645, 286);
            this.DGVStatistiqueSalles.TabIndex = 3;
            // 
            // Salle
            // 
            this.Salle.HeaderText = "Nom de la salle";
            this.Salle.Name = "Salle";
            this.Salle.Width = 200;
            // 
            // Nombre
            // 
            this.Nombre.HeaderText = "Nombre de réservations de la salle";
            this.Nombre.Name = "Nombre";
            this.Nombre.Width = 200;
            // 
            // MontantGlobal
            // 
            this.MontantGlobal.HeaderText = "Montant global de la réservation de la salle";
            this.MontantGlobal.Name = "MontantGlobal";
            this.MontantGlobal.Width = 200;
            // 
            // cbxAnnee
            // 
            this.cbxAnnee.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbxAnnee.FormattingEnabled = true;
            this.cbxAnnee.Items.AddRange(new object[] {
            "2015",
            "2016",
            "2017",
            "2018",
            "2019",
            "2020",
            "2021"});
            this.cbxAnnee.Location = new System.Drawing.Point(594, 94);
            this.cbxAnnee.Name = "cbxAnnee";
            this.cbxAnnee.Size = new System.Drawing.Size(52, 21);
            this.cbxAnnee.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(728, 290);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(352, 18);
            this.label2.TabIndex = 87;
            this.label2.Text = "Montant total des réservations éfféctuées ce mois :  ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(728, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(348, 18);
            this.label3.TabIndex = 87;
            this.label3.Text = "Nombre total des réservations éfféctuées ce mois : ";
            // 
            // textBox1
            // 
            this.textBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(1086, 207);
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(136, 24);
            this.textBox1.TabIndex = 88;
            // 
            // textBox2
            // 
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(1086, 287);
            this.textBox2.Name = "textBox2";
            this.textBox2.ReadOnly = true;
            this.textBox2.Size = new System.Drawing.Size(136, 24);
            this.textBox2.TabIndex = 88;
            // 
            // btnAfficher
            // 
            this.btnAfficher.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAfficher.Location = new System.Drawing.Point(731, 378);
            this.btnAfficher.Name = "btnAfficher";
            this.btnAfficher.Size = new System.Drawing.Size(345, 39);
            this.btnAfficher.TabIndex = 4;
            this.btnAfficher.Text = "Afficher";
            this.btnAfficher.UseVisualStyleBackColor = true;
            // 
            // frmStatistiquesSalles
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1234, 444);
            this.Controls.Add(this.btnAfficher);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.DGVStatistiqueSalles);
            this.Controls.Add(this.cbxAnnee);
            this.Controls.Add(this.cbxMois);
            this.Controls.Add(this.lblSéparationDate);
            this.Controls.Add(this.lblDate);
            this.Controls.Add(this.label1);
            this.Name = "frmStatistiquesSalles";
            this.Text = "statistiquesSalles";
            this.Load += new System.EventHandler(this.frmStatistiquesSalles_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DGVStatistiqueSalles)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblDate;
        private System.Windows.Forms.ComboBox cbxMois;
        private System.Windows.Forms.Label lblSéparationDate;
        private System.Windows.Forms.DataGridView DGVStatistiqueSalles;
        private System.Windows.Forms.ComboBox cbxAnnee;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Button btnAfficher;
        private System.Windows.Forms.DataGridViewTextBoxColumn Salle;
        private System.Windows.Forms.DataGridViewTextBoxColumn Nombre;
        private System.Windows.Forms.DataGridViewTextBoxColumn MontantGlobal;
    }
}