﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmFacturationReservations : Form
    {
        DataTable tableLigues = frmM2LReservationSalles.reservationsSallesDataSet.Tables["ligues"];
        DataTable tableReservations = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Reservations"];

        public frmFacturationReservations()
        {
            InitializeComponent();
        }

        private void frmFacturationReservations_Load(object sender, EventArgs e)
        {
            cbxLigue.DataSource = tableLigues;
            cbxLigue.DisplayMember = tableLigues.Columns[1].ToString();
            cbxLigue.ValueMember = tableLigues.Columns[0].ToString();


            for(byte i = 1; i < 13; i++)
            {
                if (i < 10)
                {
                    cbxMois.Items.Add("0" + i.ToString());
                }
                else
                {
                    cbxMois.Items.Add(i.ToString());
                }
            }
            cbxMois.SelectedItem = DateTime.Now.Month.ToString();

            short annee = Convert.ToInt16(DateTime.Now.Year);
            for(short i = annee; i > annee - 21; i--)
            {
                cbxAnnee.Items.Add(i.ToString());
            }
            cbxAnnee.SelectedItem = annee.ToString();

            dgvReservation.ColumnCount = 4;
            dgvReservation.Width = 140 * 4 + 6;

            dgvReservation.ColumnHeadersVisible = true;
            dgvReservation.Columns[0].HeaderText = "Date";
            dgvReservation.Columns[1].HeaderText = "Salle";
            dgvReservation.Columns[2].HeaderText = "Réservant";
            dgvReservation.Columns[3].HeaderText = "Montant";

            dgvReservation.ReadOnly = true;
            dgvReservation.RowHeadersVisible = false;

            for(byte i = 0; i > dgvReservation.ColumnCount; i++)
            {
                dgvReservation.ColumnHeadersDefaultCellStyle.Alignment = DataGridViewContentAlignment.MiddleCenter;
                dgvReservation.Columns[i].SortMode = DataGridViewColumnSortMode.NotSortable;
                dgvReservation.Columns[i].DefaultCellStyle.Alignment = DataGridViewContentAlignment.BottomRight;
            }
            dgvReservation.Columns[0].Width = 100;
            dgvReservation.Columns[1].Width = 180;
            dgvReservation.Columns[2].Width = 160;
            dgvReservation.Columns[3].Width = 120;

            tbxNbReservation.Text = "0";
            tbxMontant.Text = "0 €";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnAfficher_Click(object sender, EventArgs e)
        {
            int id = 0;
            foreach(DataRowView ligneTab in tableLigues.DefaultView)
            {
                if(ligneTab["nomLigue"].ToString() == cbxLigue.Text)
                {
                    id = 1;
                }
            }

            foreach (DataRowView ligneTab in tableReservations.DefaultView)
            {
                DateTime date = Convert.ToDateTime(ligneTab["DateReservation"].ToString());
            }
        }
    }
}
