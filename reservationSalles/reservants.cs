﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace reservationSalles2018
{
    public partial class frmReservants : Form
    {
        DataTable tableReservants = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Reservants"];
        DataTable tableLigues = frmM2LReservationSalles.reservationsSallesDataSet.Tables["ligues"];
        Boolean enregModifierReservant;

        private string numeroO = "";
        private string nomO = "";
        private string prenomO = "";
        private string telO = "";
        private string mailO = "";
        private string ligueO = "";
        private string fonctionO = "";

        DataRelation reservantsLigues = frmM2LReservationSalles.reservationsSallesDataSet.Relations["EquiJoinReservantLigue"];

        public frmReservants()
        {
            InitializeComponent();
           
        }

        private void reservants_Load(object sender, EventArgs e)
        {
            tbxInseeReservant.MaxLength = 15;
            tbxNomReservant.MaxLength = 40;
            tbxPrenomReservant.MaxLength = 40;
            tbxTelephoneReservant.MaxLength = 10;
            tbxMailReservant.MaxLength = 40;
            tbxFonctionReservant.MaxLength = 20;
            tbxRechercherReservant.MaxLength = 40;

            lbxReservants.DataSource = tableReservants;
            lbxReservants.DisplayMember = "nomPrenom";
            lbxReservants.ValueMember = "idReservant";
        

            cbxLigueReservant.DataSource = tableLigues;
            cbxLigueReservant.DisplayMember = tableLigues.Columns[1].ToString();
            cbxLigueReservant.ValueMember = tableLigues.Columns[0].ToString();


            tbxInseeReservant.Enabled = false;
            tbxNomReservant.Enabled = false;
            tbxPrenomReservant.Enabled = false;
            tbxTelephoneReservant.Enabled = false;
            tbxMailReservant.Enabled = false;
            cbxLigueReservant.Enabled = false;
            tbxFonctionReservant.Enabled = false;

            btnEnregistrerReservant.Visible = false;
            btnAnnulerReservant.Visible = false;

            btnRechercherReservant.Enabled = true;
        }

        private void lbxReservants_SelectedIndexChanged(object sender, EventArgs e)
        {


            if (lbxReservants.SelectedIndex != -1)
            {
                tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();

                
                cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();
                
            }
        }

        private void btnRechercherReservant_Click(object sender, EventArgs e)
        {
            int index = lbxReservants.FindString(tbxRechercherReservant.Text);
            if (index == -1)
            {
                MessageBox.Show("Reéservant introuvable.");
            }
            else
            {
                lbxReservants.SetSelected(index, true);
            }
        }


        private void btnModifierReservant_Click(object sender, EventArgs e)
        {
            if (tbxNomReservant.Text != "")
            {
                enregModifierReservant = true;
                tbxInseeReservant.Enabled = true;
                tbxNomReservant.Enabled = true;
                tbxPrenomReservant.Enabled = true;
                tbxTelephoneReservant.Enabled = true;
                tbxMailReservant.Enabled = true;
                cbxLigueReservant.Enabled = true;
                tbxFonctionReservant.Enabled = true;

                btnAjouterReservant.Visible = false;
                btnSupprimerReservant.Visible = false;
                btnModifierReservant.Enabled = false;

                btnEnregistrerReservant.Visible = true;
                btnAnnulerReservant.Visible = true;

                lbxReservants.Enabled = false;
                tbxRechercherReservant.Enabled = false;
                btnRechercherReservant.Enabled = false;

                numeroO = tbxInseeReservant.Text;
                nomO = tbxNomReservant.Text;
                prenomO = tbxPrenomReservant.Text;
                telO = tbxTelephoneReservant.Text;
                mailO = tbxMailReservant.Text;
                ligueO = cbxLigueReservant.Text;
                fonctionO = tbxFonctionReservant.Text;
            }
        }

        private void btnAnnulerReservant_Click(object sender, EventArgs e)
        {

            enregModifierReservant = false;

            tbxInseeReservant.Enabled = false;
            tbxNomReservant.Enabled = false;
            tbxPrenomReservant.Enabled = false;
            tbxTelephoneReservant.Enabled = false;
            tbxMailReservant.Enabled = false;
            cbxLigueReservant.Enabled = false;
            tbxFonctionReservant.Enabled = false;

            btnAjouterReservant.Visible = true;
            btnSupprimerReservant.Visible = true;
            btnModifierReservant.Enabled = true;

            btnEnregistrerReservant.Visible = false;
            btnAnnulerReservant.Visible = false;

            lbxReservants.Enabled = true;
            tbxRechercherReservant.Enabled = true;
            btnRechercherReservant.Enabled = true;

            if (lbxReservants.SelectedIndex != -1)
            {
                tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();


                cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();

            }
        }


        private void btnAjouterReservant_Click(object sender, EventArgs e)
        {

            tbxInseeReservant.Text = "";
            tbxNomReservant.Text = "";
            tbxPrenomReservant.Text = "";
            tbxTelephoneReservant.Text = "";
            tbxMailReservant.Text = "";
            tbxFonctionReservant.Text = "";
            cbxLigueReservant.SelectedIndex = 0;

            btnRechercherReservant.Enabled = false;
            tbxRechercherReservant.Enabled = false;

            btnAjouterReservant.Visible = false;
            btnSupprimerReservant.Visible = false;
            btnModifierReservant.Enabled = false;

            lbxReservants.Enabled = false;

            btnEnregistrerReservant.Visible = true;
            btnAnnulerReservant.Visible = true;


            enregModifierReservant = false;
            tbxInseeReservant.Enabled = true;
            tbxNomReservant.Enabled = true;
            tbxPrenomReservant.Enabled = true;
            tbxTelephoneReservant.Enabled = true;
            tbxMailReservant.Enabled = true;
            cbxLigueReservant.Enabled = true;
            tbxFonctionReservant.Enabled = true;
        }

        private void btnEnregistrerReservant_Click(object sender, EventArgs e)
        {


            int idReservant;
            short indice;

            indice = 0;


            tbxInseeReservant.Text = tbxInseeReservant.Text.Trim();
            tbxNomReservant.Text = tbxNomReservant.Text.Trim();
            tbxPrenomReservant.Text = tbxPrenomReservant.Text.Trim();
            tbxTelephoneReservant.Text = tbxTelephoneReservant.Text.Trim();
            tbxMailReservant.Text = tbxMailReservant.Text.Trim();
            tbxFonctionReservant.Text = tbxFonctionReservant.Text.Trim();

            Regex inseeRegex = new Regex(@"(1|2)[0-9]{14}");
            Regex nomRegex = new Regex(@"\p{L}+[\p{L} '-]*");
            Regex numTelRegex = new Regex(@"0[0-9]{9}");
            Regex mailRegex = new Regex(@"^[\w\p{L}]+@[\w\p{L}]+\.[a-zA-Z]{2,5}$");

            bool erreur = false;
            String msgErreur = "Voici les erreurs:";

            if (!inseeRegex.IsMatch(tbxInseeReservant.Text))
            {
                msgErreur += "\n -Le numéro d'INSEE est incorrecte!";
                erreur = true;
            }

            if (!nomRegex.IsMatch(tbxNomReservant.Text))
            {
                msgErreur += "\n -Le nom est incorrecte!";
                erreur = true;
            }

            if (!nomRegex.IsMatch(tbxPrenomReservant.Text))
            {
                msgErreur += "\n -prenom est incorrecte";
                erreur = true;
            }

            if (!numTelRegex.IsMatch(tbxTelephoneReservant.Text))
            {
                msgErreur += "\n -numéro téléphone";
                erreur = true;
            }

            if (!mailRegex.IsMatch(tbxMailReservant.Text))
            {
                msgErreur += "\n -mail est incorrecte";
                erreur = true;
            }

            if (!nomRegex.IsMatch(tbxFonctionReservant.Text))
            {
                msgErreur += "\n -fonction est incorrecte";
                erreur = true;
            }

            if (erreur)
            {
                MessageBox.Show(msgErreur);
                return;
            }

            if (enregModifierReservant)
            {
                string message = "Voulez-vous enregistré vos modification:";

                if (tbxInseeReservant.Text != numeroO)
                {
                    message += "\n-Le numero à été modifier (" + numeroO + "-- > " + tbxInseeReservant.Text + ")";
                }

                if (tbxNomReservant.Text != nomO)
                {
                    message += "\n-Le nom à été modifier (" + nomO + "-- > " + tbxNomReservant.Text + ")";
                }

                if (tbxPrenomReservant.Text != prenomO)
                {
                    message += "\n-Le prenom à été modifier (" + prenomO + "-- > " + tbxPrenomReservant.Text + ")";
                }

                if (tbxTelephoneReservant.Text != telO)
                {
                    message += "\n-Le numero à été modifier (" + prenomO + " --> " + tbxTelephoneReservant.Text + ")";
                }

                if (tbxMailReservant.Text != mailO)
                {
                    message += "\n-Le mail à été modifier (" + mailO + " --> " + tbxMailReservant.Text + ")";
                }

                if (cbxLigueReservant.Text != ligueO)
                {
                    message += "\n-La ligue à été modifier (" + ligueO + " --> " + cbxLigueReservant.Text + ")";
                }

                if (tbxFonctionReservant.Text != fonctionO)
                {
                    message += "\n-La fonctionn à été modifier (" + fonctionO + " --> " + tbxFonctionReservant.Text + ")";
                }


                var confirmModif = MessageBox.Show(message, "Confirmation des modifications", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (confirmModif == DialogResult.No)
                {
                    return;
                }
            }

            try
                {

                    if (enregModifierReservant == false)
                    {
                        dbConnexion.ajouterReservant(tbxInseeReservant.Text, tbxNomReservant.Text, tbxPrenomReservant.Text, tbxTelephoneReservant.Text, tbxMailReservant.Text, Convert.ToInt32(cbxLigueReservant.SelectedValue), tbxFonctionReservant.Text);
                    }
                    else
                    {
                        idReservant = Convert.ToInt32(tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[0]);
                        dbConnexion.modifierReservant(idReservant, tbxInseeReservant.Text, tbxNomReservant.Text, tbxPrenomReservant.Text, tbxTelephoneReservant.Text, tbxMailReservant.Text, Convert.ToInt32(cbxLigueReservant.SelectedValue), tbxFonctionReservant.Text);

                        indice = Convert.ToInt16(lbxReservants.SelectedIndex);

                    }

                    dbConnexion.miseJourDataSet();
                  
                    lbxReservants.DataSource = tableReservants;
                    lbxReservants.DisplayMember = "nomPrenom";


                    if (enregModifierReservant == true)
                    {
                        lbxReservants.SelectedIndex = indice;
                        enregModifierReservant = false;
                    }
                    else
                    {
                        lbxReservants.SelectedIndex = lbxReservants.Items.Count - 1;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                tbxInseeReservant.Enabled = false;
                tbxNomReservant.Enabled = false;
                tbxPrenomReservant.Enabled = false;
                tbxTelephoneReservant.Enabled = false;
                tbxMailReservant.Enabled = false;
                cbxLigueReservant.Enabled = false;
                tbxFonctionReservant.Enabled = false;

                btnAjouterReservant.Visible = true;
                btnSupprimerReservant.Visible = true;
                btnModifierReservant.Enabled = true;

                btnEnregistrerReservant.Visible = false;
                btnAnnulerReservant.Visible = false;

                lbxReservants.Enabled = true;
                tbxRechercherReservant.Enabled = true;
                btnRechercherReservant.Enabled = true;

                if (lbxReservants.SelectedIndex != -1)
                {
                    tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                    tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                    tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                    tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                    tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                    tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();


                    cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();

                }
            
        }

        private void btnSupprimerReservant_Click(object sender, EventArgs e)
        {

            int idReservant;
            const string messageSupp = "Voulez vous vraiment supprimer ce reservant";
            const string titre = "Supprimer Reservant";

            var resultUtilisateur = MessageBox.Show(messageSupp, titre, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (resultUtilisateur == DialogResult.Yes)
            {
                if (lbxReservants.SelectedIndex >= 0)
                {
                    idReservant = Convert.ToInt32(tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[0]);
                    dbConnexion.supprimerReservant(idReservant);
                    dbConnexion.miseJourDataSet();

                    if (lbxReservants.SelectedIndex != -1)
                    {
                        tbxInseeReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[1].ToString();
                        tbxNomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[2].ToString();
                        tbxPrenomReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[3].ToString();
                        tbxTelephoneReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[4].ToString();
                        tbxMailReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[5].ToString();
                        tbxFonctionReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].ItemArray[7].ToString();

                        cbxLigueReservant.Text = tableReservants.Rows[lbxReservants.SelectedIndex].GetParentRow(reservantsLigues)["nomLigue"].ToString();
                    }
                    else
                    {
                        tbxInseeReservant.Text = "";
                        tbxNomReservant.Text = "";
                        tbxPrenomReservant.Text = "";
                        tbxTelephoneReservant.Text = "";
                        tbxMailReservant.Text = "";
                        tbxFonctionReservant.Text = "";
                        cbxLigueReservant.SelectedIndex = 0;
                    }
                }
                else
                {
                    MessageBox.Show("Vous devez sélectionner un réservant");
                }
            }
        }
    }
}
