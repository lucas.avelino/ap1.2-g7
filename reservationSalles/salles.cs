﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace reservationSalles2018
{
    public partial class frmSalles : Form
    {
        DataTable tableSalles , tableTypes;
       
        Boolean enregModifierSalle;

        private string nomSalleO = "";
        private string typeSalleO = "";
        private string surfaceO = "";
        private string capaciteO = "";
        private string prixLocationO = "";

        DataRelation sallesTypes = frmM2LReservationSalles.reservationsSallesDataSet.Relations["EquiJoinSalleType"];


        public frmSalles()
        {
            InitializeComponent();
        }

        private void salles_Load(object sender, EventArgs e)
        {
            tableSalles = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Salles"];
            tableTypes = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Types"];
         

            lbxSalles.DataSource = tableSalles;
            lbxSalles.DisplayMember = tableSalles.Columns[1].ToString();
            lbxSalles.ValueMember = tableSalles.Columns[0].ToString();

            cbxTypeSalle.DataSource = tableTypes;
            cbxTypeSalle.DisplayMember = tableTypes.Columns[1].ToString();
            cbxTypeSalle.ValueMember = tableTypes.Columns[0].ToString();

            tbxNomSalle.Enabled = false;
            cbxTypeSalle.Enabled = false;
            tbxSurfaceSalle.Enabled = false;
            tbxCapaciteSalle.Enabled = false;
            tbxPrixLocationSalle.Enabled = false;

            btnEnregistrerSalle.Visible = false;
            btnAnnulerSalle.Visible = false;
            //Nombre de caractères maximum par textBox
            tbxNomSalle.MaxLength = 40;
            tbxRechercherSalle.MaxLength = 40;
            cbxTypeSalle.MaxLength = 20;
            tbxSurfaceSalle.MaxLength = 3;
            tbxCapaciteSalle.MaxLength = 3;
            tbxPrixLocationSalle.MaxLength = 8;
        }

        private void lbxSalles_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxSalles.SelectedIndex != -1)
            {
                tbxNomSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[1].ToString();
                cbxTypeSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].GetParentRow(sallesTypes)["libelleType"].ToString();
                tbxSurfaceSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[3].ToString();
                tbxCapaciteSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[4].ToString();
                tbxPrixLocationSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[5].ToString();
            }
        }

        private void btnRechercheSalle_Click(object sender, EventArgs e)
        {
            int index = lbxSalles.FindString(tbxRechercherSalle.Text);
            if (index == -1)
            {
                MessageBox.Show("Salle introuvable.");
            }
            else
            {
                lbxSalles.SetSelected(index, true);
            }
        }

        private void btnModifierSalle_Click(object sender, EventArgs e)
        {
            if (tbxNomSalle.Text != "")
            {
                enregModifierSalle = true;
                tbxNomSalle.Enabled = true;
                cbxTypeSalle.Enabled = true;
                tbxSurfaceSalle.Enabled = true;
                tbxCapaciteSalle.Enabled = true;
                tbxPrixLocationSalle.Enabled = true;

                btnAjouterSalle.Visible = false;
                btnSupprimerSalle.Visible = false;
                btnModifierSalle.Enabled = false;

                btnEnregistrerSalle.Visible = true;
                btnAnnulerSalle.Visible = true;

                lbxSalles.Enabled = false;
                tbxRechercherSalle.Enabled = false;

                nomSalleO = tbxNomSalle.Text;
                typeSalleO = cbxTypeSalle.Text;
                capaciteO = tbxCapaciteSalle.Text;
                prixLocationO = tbxPrixLocationSalle.Text;
            }
        }

        private void btnAnnulerSalle_Click(object sender, EventArgs e)
        {
            enregModifierSalle = false;

            tbxNomSalle.Enabled = false;
            cbxTypeSalle.Enabled = false;
            tbxSurfaceSalle.Enabled = false;
            tbxCapaciteSalle.Enabled = false;
            tbxPrixLocationSalle.Enabled = false;

            btnAjouterSalle.Visible = true;
            btnSupprimerSalle.Visible = true;
            btnModifierSalle.Enabled = true;

            btnEnregistrerSalle.Visible = false;
            btnAnnulerSalle.Visible = false;

            lbxSalles.Enabled = true;
            tbxRechercherSalle.Enabled = true;

            if (lbxSalles.SelectedIndex != -1)
            {
                tbxNomSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[1].ToString();
                cbxTypeSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].GetParentRow(sallesTypes)["libelleType"].ToString();
                tbxSurfaceSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[3].ToString();
                tbxCapaciteSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[4].ToString();
                tbxPrixLocationSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[5].ToString();
            }
        }

        private void btnAjouterSalle_Click(object sender, EventArgs e)
        {
            tbxNomSalle.Text = "";
            cbxTypeSalle.SelectedIndex = 0;
            tbxSurfaceSalle.Text = "";
            tbxCapaciteSalle.Text = "";
            tbxPrixLocationSalle.Text = "";


            tbxNomSalle.Enabled = true;
            cbxTypeSalle.Enabled = true;
            tbxSurfaceSalle.Enabled = true;
            tbxCapaciteSalle.Enabled = true;
            tbxPrixLocationSalle.Enabled = true;

            btnAjouterSalle.Visible = false;
            btnSupprimerSalle.Visible = false;
            btnModifierSalle.Enabled = false;

            btnEnregistrerSalle.Visible = true;
            btnAnnulerSalle.Visible = true;

            lbxSalles.Enabled = false;
            tbxRechercherSalle.Enabled = false;
        }

        private void btnEnregistrerSalle_Click(object sender, EventArgs e)
        {

            int surface;
            int capacite;
            float prix;

            int idSalle;
            short indice;

            if(float.TryParse(tbxPrixLocationSalle.Text, out prix))
            {
                if (prix < 0 || prix > 10000)
                {
                    tbxPrixLocationSalle.Text = null;
                    MessageBox.Show("Le prix n'est pas bon");
                }
                else
                {
                    tbxPrixLocationSalle.Text = Convert.ToString(Math.Round(prix, 2));
                }
            }
            else
            {
                tbxPrixLocationSalle.Text = null;
                MessageBox.Show("Le prix n'est pas bon");
            }

            indice = 0;

            if (int.TryParse(tbxSurfaceSalle.Text, out surface))
            {
                if (surface <= 0 || surface > 400)
                {
                    //MessageBox.Show("Erreur Surface salle");
                    tbxSurfaceSalle.Text = null;
                }
            }
            else
            {
                //MessageBox.Show("Erreur Surface salle");
                tbxSurfaceSalle.Text = null;
            }

            if (int.TryParse(tbxCapaciteSalle.Text, out capacite))
            {
                if (capacite <= 0 || capacite > 300)
                {
                    //MessageBox.Show("Erreur Capacité salle");
                    tbxCapaciteSalle.Text = null;
                }
            }
            else
            {
                //MessageBox.Show("Erreur Capacité salle");
                tbxSurfaceSalle.Text = null;
            }

            if (enregModifierSalle)
            {
                string message = "Voulez-vous enregistré vos modification";

                if (tbxNomSalle.Text != nomSalleO)
                {
                    message += "\n-Le nom à été modifier (" + nomSalleO + " --> " + tbxNomSalle.Text + ")";
                }

                if (cbxTypeSalle.Text != typeSalleO)
                {
                    message += "\n-Le type de salle à été modifier (" + typeSalleO + " --> " + cbxTypeSalle.Text + ")";
                }

                if (tbxCapaciteSalle.Text != capaciteO)
                {
                    message += "\n-La capaité de la salle à été modifier (" + capaciteO + " --> " + tbxCapaciteSalle.Text + ")";
                }

                if (tbxPrixLocationSalle.Text != prixLocationO)
                {
                    message += "\n-Le prix à été modifier (" + prixLocationO + " --> " + tbxPrixLocationSalle.Text + ")";
                }

                var confirmModif = MessageBox.Show(message, "Confirmation des modifications", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (confirmModif == DialogResult.No)
                {
                    return;
                }
            }

            try
            {

                if (enregModifierSalle == false)
                {
                    dbConnexion.ajouterSalle(tbxNomSalle.Text, Convert.ToInt32(cbxTypeSalle.SelectedValue), Convert.ToInt16(tbxSurfaceSalle.Text), Convert.ToInt16(tbxCapaciteSalle.Text), Convert.ToSingle(tbxPrixLocationSalle.Text));
                }
                else
                {
                    idSalle = Convert.ToInt32(tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[0]);
                    dbConnexion.modifierSalle(idSalle, tbxNomSalle.Text, Convert.ToInt32(cbxTypeSalle.SelectedValue), Convert.ToInt16(tbxSurfaceSalle.Text), Convert.ToInt16(tbxCapaciteSalle.Text), Convert.ToSingle(tbxPrixLocationSalle.Text));
                    indice = Convert.ToInt16(lbxSalles.SelectedIndex);
                }

                dbConnexion.miseJourDataSet();
                lbxSalles.DataSource = tableSalles;
                lbxSalles.DisplayMember = tableSalles.Columns[1].ToString();

                if (enregModifierSalle == true)
                {
                    lbxSalles.SelectedIndex = indice;
                    enregModifierSalle = false;
                }
                else
                {
                    lbxSalles.SelectedIndex = lbxSalles.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


            tbxNomSalle.Enabled = false;
            cbxTypeSalle.Enabled = false;
            tbxSurfaceSalle.Enabled = false;
            tbxCapaciteSalle.Enabled = false;
            tbxPrixLocationSalle.Enabled = false;

            btnAjouterSalle.Visible = true;
            btnSupprimerSalle.Visible = true;
            btnModifierSalle.Enabled = true;

            btnEnregistrerSalle.Visible = false;
            btnAnnulerSalle.Visible = false;

            lbxSalles.Enabled = true;
            tbxRechercherSalle.Enabled = true;
            if (lbxSalles.SelectedIndex != -1)
            {
                tbxNomSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[1].ToString();
                cbxTypeSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].GetParentRow(sallesTypes)["libelleType"].ToString();
                tbxSurfaceSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[3].ToString();
                tbxCapaciteSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[4].ToString();
                tbxPrixLocationSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[5].ToString();


            }
        }
        private void tbxNomSalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Space && e.KeyChar != '\'' && e.KeyChar != '-' ) 
            {
                e.Handled = true;
            }
        }

        private void cbxTypeSalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxSurfaceSalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }

        }

        private void tbxCapaciteSalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void tbxPrixLocationSalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != ',')
            {
                e.Handled = true;
            }
        }

        private void tbxRechercherSalle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Space && e.KeyChar != '\'' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void btnSupprimerSalle_Click(object sender, EventArgs e)
        {
            int idSalle;

            const string messageSupp = "Voulez vous vraiment supprimer cette salle";
            const string titre = "Supprimer Salles";

            var resultUtilisateur = MessageBox.Show(messageSupp, titre, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (resultUtilisateur == DialogResult.Yes)
            {
                try
                {
                    if (lbxSalles.SelectedIndex >= 0)
                    {
                        idSalle = Convert.ToInt32(tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[0]);
                        dbConnexion.supprimerSalle(idSalle);
                        dbConnexion.miseJourDataSet();

                        if (lbxSalles.SelectedIndex != -1)
                        {
                            tbxNomSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[1].ToString();
                            cbxTypeSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].GetParentRow(sallesTypes)["libelleType"].ToString();
                            tbxSurfaceSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[3].ToString();
                            tbxCapaciteSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[4].ToString();
                            tbxPrixLocationSalle.Text = tableSalles.Rows[lbxSalles.SelectedIndex].ItemArray[5].ToString();

                        }
                        else
                        {
                            tbxNomSalle.Text = "";
                            cbxTypeSalle.Text = "";
                            tbxSurfaceSalle.Text = "";
                            tbxCapaciteSalle.Text = "";
                            tbxPrixLocationSalle.Text = "";
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vous devez sélectionner une salle");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
            
        }
    }
}
