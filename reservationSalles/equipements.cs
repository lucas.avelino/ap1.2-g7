﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmEquipements : Form
    {

        DataTable tableEquipements;

        Boolean enregModifierEquipement;

        private string nomO = "";
        private string quantiteO = "";
        private string prixO = "";

        public frmEquipements()
        {
            InitializeComponent();
        }

        private void frmEquipements_Load(object sender, EventArgs e)
        {
            tableEquipements = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Equipements"];



            lbxEquipements.DataSource = tableEquipements;
            lbxEquipements.DisplayMember = tableEquipements.Columns[1].ToString();
            lbxEquipements.ValueMember = tableEquipements.Columns[0].ToString();

            tbxNomEquipement.Enabled = false;
            tbxQuantiteEquipement.Enabled = false;
            tbxPrixEquipement.Enabled = false;


            btnEnregistrerEquipement.Visible = false;
            btnAnnulerEquipement.Visible = false;

            tbxRechercherEquipement.MaxLength = 40;
            tbxNomEquipement.MaxLength = 40;
            tbxQuantiteEquipement.MaxLength = 3;
        }

       

        private void lbxEquipements_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (lbxEquipements.SelectedIndex != -1)
            {
                tbxNomEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[1].ToString();
                tbxQuantiteEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[2].ToString();
                tbxPrixEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[3].ToString();

            }
        }

        private void btnAjouterEquipement_Click(object sender, EventArgs e)
        {
            tbxNomEquipement.Text = "";
            tbxQuantiteEquipement.Text = "";
            tbxPrixEquipement.Text = "";



            tbxNomEquipement.Enabled = true;
            tbxQuantiteEquipement.Enabled = true;
            tbxPrixEquipement.Enabled = true;
           


            btnAjouterEquipement.Visible = false;
            btnSupprimerEquipement.Visible = false;
            btnModifierEquipement.Enabled = false;
            tbxRechercherEquipement.Enabled = false;
            btnRechercherEquipement.Enabled = false;

            lbxEquipements.Enabled = false;

            btnEnregistrerEquipement.Visible = true;
            btnAnnulerEquipement.Visible = true;


            enregModifierEquipement = false;
        }

        private void btnEnregistrerEquipement_Click(object sender, EventArgs e)
        {
            int idEquipement;
            int quantite;
            short indice;
            float prix;

            indice = 0;


            tbxNomEquipement.Text = tbxNomEquipement.Text.Trim();
            tbxQuantiteEquipement.Text = tbxQuantiteEquipement.Text.Trim();
            tbxPrixEquipement.Text = tbxPrixEquipement.Text.Trim();

            bool erreur = false;
            string msgErreur = "Voici les erreurs:"

            if (int.TryParse(tbxQuantiteEquipement.Text, out quantite))
            {
                if (quantite <= 0 || quantite >= 1000)
                {
                    tbxQuantiteEquipement.Text = null;
                    msgErreur += "La quantité n'est pas bonne";
                }
            }
            else
            {
                tbxQuantiteEquipement.Text = null;
                msgErreur += "La quantité n'est pas bonne";
            }
            if (Single.TryParse(tbxPrixEquipement.Text, out prix))
            {
                if (prix < 0 || prix > 10000)
                {
                    tbxPrixEquipement.Text = null;
                    msgErreur += "Le prix n'est pas bon";
                }
                else
                {
                    tbxPrixEquipement.Text = Convert.ToString(Math.Round(prix, 2));
                }
            }
            else
            {
                tbxPrixEquipement.Text = null;
                msgErreur += "Le prix n'est pas bon";
            }

            if (enregModifierEquipement)
            {
                string message = "Voulez-vous enregistrer vos modification:";

                if (tbxNomEquipement.Text != nomO)
                {
                    message += "\n-Le nom à été modifier (" + nomO + " --> " + tbxNomEquipement.Text + ")";
                }

                if (tbxQuantiteEquipement.Text != quantiteO)
                {
                    message += "\n-La quanite à été modifier (" + quantiteO + " --> " + tbxQuantiteEquipement.Text + ")";
                }

                if (tbxPrixEquipement.Text != prixO)
                {
                    message += "\n-Le prix à été modifier (" + prixO + " --> " + tbxPrixEquipement.Text + ")";
                }

                var confirmModif = MessageBox.Show(message, "Confirmation des modifications", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                if (confirmModif == DialogResult.No)
                {
                    return;
                }

            }

            if (int.TryParse(tbxQuantiteEquipement.Text, out quantite))
            {
                if(quantite <=0 || quantite >= 1000)
                {
                    tbxQuantiteEquipement.Text = null;
                    MessageBox.Show("La quantité n'est pas bonne");
                }
            }
            else
            {
                tbxQuantiteEquipement.Text = null;
                MessageBox.Show("La quantité n'est pas bonne");
            }
            if (Single.TryParse(tbxPrixEquipement.Text, out prix))
            {
                if (prix < 0 || prix > 10000)
                {
                    tbxPrixEquipement.Text = null;
                    MessageBox.Show("Le prix n'est pas bon");
                }
                else
                {
                    tbxPrixEquipement.Text = Convert.ToString(Math.Round(prix, 2));
                }
            }
            else
            {
                tbxPrixEquipement.Text = null;
                MessageBox.Show("Le prix n'est pas bon");
            }


            try
            {

                if (enregModifierEquipement == false)
                {
                    dbConnexion.ajouterEquipement(tbxNomEquipement.Text, Convert.ToInt32(tbxQuantiteEquipement.Text), Convert.ToSingle(tbxPrixEquipement.Text));
                }
                else
                {
                    idEquipement = Convert.ToInt32(tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[0]);
                    dbConnexion.modifierEquipement(idEquipement, tbxNomEquipement.Text, Convert.ToInt32(tbxQuantiteEquipement.Text), Convert.ToSingle(tbxPrixEquipement.Text));
                    indice = Convert.ToInt16(lbxEquipements.SelectedIndex);

                }

                dbConnexion.miseJourDataSet();

                lbxEquipements.DataSource = tableEquipements;
                lbxEquipements.DisplayMember = tableEquipements.Columns[1].ToString();
                lbxEquipements.ValueMember = tableEquipements.Columns[0].ToString();


                if (enregModifierEquipement == true)
                {
                    lbxEquipements.SelectedIndex = indice;
                    enregModifierEquipement = false;
                }
                else
                {
                    lbxEquipements.SelectedIndex = lbxEquipements.Items.Count - 1;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            tbxNomEquipement.Enabled = false;
            tbxQuantiteEquipement.Enabled = false;
            tbxPrixEquipement.Enabled = false;


            btnAjouterEquipement.Visible = true;
            btnSupprimerEquipement.Visible = true;
            btnModifierEquipement.Enabled = true;

            btnEnregistrerEquipement.Visible = false;
            btnAnnulerEquipement.Visible = false;

            lbxEquipements.Enabled = true;
            tbxRechercherEquipement.Enabled = true;
            btnRechercherEquipement.Enabled = true;

            if (lbxEquipements.SelectedIndex != -1)
            {
                tbxNomEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[1].ToString();
                tbxQuantiteEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[2].ToString();
                tbxPrixEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[3].ToString();

            } 
        }

        private void btnAnnulerEquipement_Click(object sender, EventArgs e)
        {
            enregModifierEquipement = false;

            tbxNomEquipement.Enabled = false;
            tbxQuantiteEquipement.Enabled = false;
            tbxPrixEquipement.Enabled = false;

            btnAjouterEquipement.Visible = true;
            btnSupprimerEquipement.Visible = true;
            btnModifierEquipement.Enabled = true;

            btnEnregistrerEquipement.Visible = false;
            btnAnnulerEquipement.Visible = false;

            lbxEquipements.Enabled = true;
            tbxRechercherEquipement.Enabled = true;
            btnRechercherEquipement.Enabled = true;

            if (lbxEquipements.SelectedIndex != -1)
            {
                tbxNomEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[1].ToString();
                tbxQuantiteEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[2].ToString();
                tbxPrixEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[3].ToString();
            }
        }

        private void btnModifierEquipement_Click(object sender, EventArgs e)
        {
            if (tbxNomEquipement.Text != "")
            {
                enregModifierEquipement = true;
                tbxNomEquipement.Enabled = true;
                tbxQuantiteEquipement.Enabled = true;
                tbxPrixEquipement.Enabled = true;


                btnAjouterEquipement.Visible = false;
                btnSupprimerEquipement.Visible = false;
                btnModifierEquipement.Enabled = false;

                btnEnregistrerEquipement.Visible = true;
                btnAnnulerEquipement.Visible = true;

                lbxEquipements.Enabled = false;
                tbxRechercherEquipement.Enabled = false;
                btnRechercherEquipement.Enabled = false;

                nomO = tbxNomEquipement.Text;
                quantiteO = tbxQuantiteEquipement.Text;
                prixO = tbxPrixEquipement.Text;
            }
        }

        private void btnSupprimerEquipement_Click(object sender, EventArgs e)
        {
            int idEquipement;
            const string messageSupp = "Voulez vous vraiment supprimer cet equipement";
            const string titre = "Supprimer Equipement";

            var resultUtilisateur = MessageBox.Show(messageSupp, titre, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (resultUtilisateur == DialogResult.Yes)
            {
                try
                {
                    if (lbxEquipements.SelectedIndex >= 0)
                    {
                        idEquipement = Convert.ToInt32(tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[0]);
                        dbConnexion.supprimerEquipement(idEquipement);
                        dbConnexion.miseJourDataSet();

                        if (lbxEquipements.SelectedIndex != -1)
                        {
                            tbxNomEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[1].ToString();
                            tbxQuantiteEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[2].ToString();
                            tbxPrixEquipement.Text = tableEquipements.Rows[lbxEquipements.SelectedIndex].ItemArray[3].ToString();

                        }
                        else
                        {
                            tbxNomEquipement.Text = "";
                            tbxQuantiteEquipement.Text = "";
                            tbxPrixEquipement.Text = "";
                        }
                    }
                    else
                    {
                        MessageBox.Show("Vous devez sélectionner un équipement");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);

                }
            }
        }

        private void tbxNomEquipement_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && (!char.IsDigit(e.KeyChar)) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Space && e.KeyChar != '\'' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void tbxRechercherEquipement_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsLetter(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != (char)Keys.Space && e.KeyChar != '\'' && e.KeyChar != '-')
            {
                e.Handled = true;
            }
        }

        private void tbxPrixEquipement_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back && e.KeyChar != ',')
            {
                e.Handled = true;
            }
        }

        private void tbxQuantiteEquipement_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (char)Keys.Back)
            {
                e.Handled = true;
            }
        }
    }
}
