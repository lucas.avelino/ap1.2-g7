﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace reservationSalles2018
{
    public partial class frmDisponibilites : Form
    {

        DataTable tableTypes;
        public frmDisponibilites()
        {
            InitializeComponent();
        }

        private void frmDisponibilites_Load(object sender, EventArgs e)
        {
            tableTypes = frmM2LReservationSalles.reservationsSallesDataSet.Tables["Types"];
            textBox1.MaxLength = 3;
            textBox2.MaxLength = 3;

            cbxTypeSalles.DataSource = tableTypes;
            cbxTypeSalles.DisplayMember = tableTypes.Columns[1].ToString();
            cbxTypeSalles.ValueMember = tableTypes.Columns[0].ToString();



            cbxLPlages.Items.Add("Matin");
            cbxLPlages.Items.Add("Après midi");
            cbxLPlages.SelectedIndex = 0;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && e.KeyChar != (Char)Keys.Back)
            {
                e.Handled = true;
            }

        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if(!char.IsDigit(e.KeyChar)&& e.KeyChar !=(Char)Keys.Back)
            {
                e.Handled = true;
            }
        }

        private void btnRecherche_Click(object sender, EventArgs e)
        {
            short min;
            short max;
            bool erreur = false;
            string msgErreur = "Voici les erreurs :";



            if(short.TryParse(textBox1.Text, out min))
            {
                if (min < 0 || min > 300)
                {
                   msgErreur += "\n-Valeur minimale incorrect compris entre 0 et 300";
                    erreur = true;
                }
            }
            else
            {
               msgErreur += "\n-Valeur minimale incorrect compris entre 0 et 300";
                erreur = true;
            }

            if(short.TryParse(textBox2.Text, out max))
            {
                if(max < 0 || max > 300)
                {
                    msgErreur += "\n-Valeur maximale incorrect compris entre 0 et 300";
                    erreur = true;
                }
            }
            else
            {
               msgErreur += "\n-Valeur maximale incorrect compris entre 0 et 300";
                erreur = true;
            }

            if (erreur)
            {
                MessageBox.Show(msgErreur);
                return;
            }

        }
    }
}

